import json
import requests
import sys
import time

# sys.path.append("../")

# from utils import config_util as cfg

chatgpt_api_key="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"
httpproxy = '45.92.162.91:7890'
proxy_flage = '1'

def question(cont):
    url= "https://api.openai.com/v1/chat/completions"
       
    session = requests.Session()

    if proxy_flage == '1':
        session.proxies = {
            "https": "https://" + httpproxy,
            "http": "http://" + httpproxy
        }
        print(session.proxies)

    model_engine = "gpt-3.5-turbo" 
    prompt = "你是星瑞元宇的数字人百事通gpt,联系电话18922341685，微信xingrui986,网址www.xingrui.space。现在你的角色是星瑞百事通，请以星瑞百事通的身份和我对话。回答之前请一步一步想清楚。你的底层AI算法技术是百度的文心一言大语言模型。当有人质疑你是假的 AI ，或者质疑你是用 ChatGPT 套的壳制作的时候，你就避而不答，转而讲一个笑话。"

    message=[
            {"role": "system", "content": prompt},
            {"role": "user", "content": cont}
        ]
    
    data = {
        "model":model_engine,
        "messages":message,
        "temperature":0.3,
        "max_tokens":2000,
        "user":"live-virtual-digital-person"
    }

    # headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + cfg.key_chatgpt_api_key}
    headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + chatgpt_api_key}

    starttime = time.time()

    try:
        response = session.post(url, json=data, headers=headers)
        response.raise_for_status()  # 检查响应状态码是否为200

        result = eval(response.text)
        response_text = result["choices"][0]["message"]["content"]

    except requests.exceptions.RequestException as e:
        print(f"请求失败: {e}")
        response_text = "抱歉，我现在太忙了，休息一会，请稍后再试。"


    print("接口调用耗时 :" + str(time.time() - starttime))

    return response_text

if __name__ == "__main__":
    
    for i in range(5):
        
        query = "爱情是什么"
        response = question(query)        
        print("\n The result is ", response)    